/*
* SPI_Manager.h
* Author: Roman
*/


#ifndef __SPI_MANAGER_H__
#define __SPI_MANAGER_H__

#include <avr/io.h>
#include <asf.h>
#include "../Tools/Tools.h"
using namespace std;

class SPI_Manager
{
	//variables
	public:
	SPI_t *spi; //&SPIC or &SPID etc
	struct spi_device spi_device_conf = {
		.id = 0
	};
	protected:
	private:	
	
	//functions
	public:
	SPI_Manager();
	~SPI_Manager();
	
	/**
	* Initialise SPI interface in master mode
	* @return The error code
	*/
	int Init_master(int chipID);
	
	/**
	* Select a slave device
	* @param spi SPI port of the Xmega microcontroller.	
	* @param slave device
	* @return The error code
	*/
	int Select_device(SPI_t *spi/*SPID or SPIC*/, spi_device *device);
	
	/**
	* Deselect a slave device
	* @param spi SPI port of the Xmega microcontroller.	
	* @param slave device
	* @return The error code
	*/
	int Deselect_device(SPI_t *spi, spi_device *device);
	
	/**
	* Read a register of LMH0384 equlizer
	* @param spi SPI port of the Xmega microcontroller.	
	* @param chipID id for a slave device (in this case - LMH0384)
	* @param regAddress the address of the destination register
	* @param buf buffer to write the read data 
	* @return The error code
	*/
	int Read_LMH3084_register(SPI_t *spi, int chipID, uint8_t* regAddress, uint8_t* buf, uint8_t* bitMask);
	
	/**
	* Write to a register of LMH0384 equlizer
	* @param spi SPI port of the Xmega microcontroller.	
	* @param chipID id for a slave device (in this case - LMH0384)
	* @param regAddress the address of the destination register
	* @param dataBuffer buffer with data to write to the register 
	* @param bitMask mask for selecting the field bits among all of the register bits
	* @return The error code
	*/
	int Write_LMH0384_register(SPI_t *spi, int chipID, uint8_t* regAddress, uint8_t* dataBuffer, uint8_t* bitMask);
	
	protected:
	private:
	/**
	* Initialise SPI pins at Xmega 	
	* @return The error code
	*/
	int Spi_init_pins(void);
	/**
	* Initialise the master device logic
	* @return The error code
	*/
	int Spi_init_module(int chipID);	
	
	/**
	* Write a packet via SPI
	* @param spi SPI port of the Xmega microcontroller.	
	* @param data data buffer
	* @param data size of data buffer  
	* @return The error code
	*/
	int Write_packet(SPI_t *spi, uint8_t *data, size_t len);
	
	/**
	* Read a packet via SPI
	* @param spi SPI port of the Xmega microcontroller.	
	* @param data data buffer
	* @param data size of data buffer  
	* @return The error code
	*/
	int Read_packet(SPI_t *spi, uint8_t *data, size_t len);

	status_code_t spi_read_packetTry(SPI_t *spi, uint8_t* regAddress, uint8_t *readBuffer, size_t len);

	SPI_Manager( const SPI_Manager &c );
	SPI_Manager& operator=( const SPI_Manager &c );

}; //SPI_Manager

#endif //__SPI_MANAGER_H__
