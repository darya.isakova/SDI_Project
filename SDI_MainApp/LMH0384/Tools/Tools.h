/*
* Tools.h
*
* Created: 10/25/2016 7:03:01 PM
* Author: Roman
*/


#ifndef __TOOLS_H__
#define __TOOLS_H__

#include <stdio.h>
#include <string.h>

using namespace std;

class Tools
{
	//variables
	public:
	protected:
	private:

	//functions
	public:
	Tools();
	~Tools();

	static int Calculate_cable_length_from_coeff(uint8_t coeff, double* res);
	static char GetBit(uint8_t* bitArray, int index);
	static int SetBit(uint8_t* bitArray, int index, uint8_t value);

	protected:
	private:


}; //Tools

#endif //__TOOLS_H__
