/*
* SDI_Manager.cpp
*
* Created: 10/27/2016 8:43:45 AM
* Author: darya
*/


#include "SDI_Manager.h"

	LMH0303_Manager* SDI_Manager::TWI_manager;
	LMH0384_Chip* SDI_Manager::lmh0384_SPI_manager;
	
/*
SDI_Manager::SDI_Manager(){
	TWI_manager = new LMH0303_Manager(/ *5* /1);
	lmh0384_SPI_manager = new LMH0384_Chip(); //set ChipID!
}*/

void SDI_Manager::Initialize(){
	//for API and  TWI
	sysclk_init();
	board_init();
	
	LMH0303_Manager::Initialize();
	lmh0384_SPI_manager->initChip(1);
	
	TWI_manager = new LMH0303_Manager(1);
	lmh0384_SPI_manager = new LMH0384_Chip();
}

int SDI_Manager::setDefaults(){
	int error1 = setLMH0303Defaults();
	int error2 = setLMH0384Defaults();
	
	return error1<0?error1: error2;
}

int SDI_Manager::setLMH0303Defaults(){
	int error = TWI_Chip::changeDefaultAddresses();
	//set defaults for LMH0303 come from San Diego
	return error;
}

int SDI_Manager::setLMH0384Defaults(){
	//set defaults for LMH0384 come from San Diego
	return 0;
}

int SDI_Manager::parseCommand(Command* cmd){
	int error = 0;
	if(strstr(cmd->chip, "LMH0303")){
		error = parseCommandLMH0303(cmd);
	}
	else
	if(strstr(cmd->chip, "LMH0384")){
		error = parseCommandLMH0384(cmd);
	}

	else return -50;
	
	return error;
}

int SDI_Manager::parseCommandLMH0303(Command* cmd){
	int error = 0;
	char* reg = cmd->reg;
	char* field = cmd->reg_field;
	bool read = cmd->read;
	char* empty = (char*)malloc(REG_NAME_LENGTH);
	
	//Set TWI_manager chip and call read/write functions
	int chipID = cmd->chipID;
	if(chipID < 0 || chipID > 5) return -51;
	TWI_manager->setChipID(chipID);
	
	if(strstr(reg, "DeviceID")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "DevID") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::DeviceID, DeviceIDRegister::DevID, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::DeviceID, DeviceIDRegister::DevID, cmd->value);
			}
			else if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::DeviceID, DeviceIDRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::DeviceID, DeviceIDRegister::RSVD, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::DeviceID, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::DeviceID, cmd->value);
		}
	}
	else if(strstr(reg, "Status")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Status, StatusRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Status, StatusRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "TFN") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Status, StatusRegister::TFN, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Status, StatusRegister::TFN, cmd->value);
			}
			else if(strstr(field, "TFP") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Status, StatusRegister::TFP, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Status, StatusRegister::TFP, cmd->value);
			}
			else if(strstr(field, "LOS") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Status, StatusRegister::LOS, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Status, StatusRegister::LOS, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Status, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Status, cmd->value);
		}
	}
	else if(strstr(reg, "Mask")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "SD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::SD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::SD, cmd->value);
			}
			else if(strstr(field, "RSVD6") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::RSVD6, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::RSVD6, cmd->value);
			}
			else if(strstr(field, "PD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::PD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::PD, cmd->value);
			}
			else if(strstr(field, "RSVD43") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::RSVD43, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::RSVD43, cmd->value);
			}
			else if(strstr(field, "MTFN") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::MTFN, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::MTFN, cmd->value);
			}
			else if(strstr(field, "MTFP") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::MTFP, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::MTFP, cmd->value);
			}
			else if(strstr(field, "MLOS") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Mask, MaskRegister::MLOS, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Mask, MaskRegister::MLOS, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Mask, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Mask, cmd->value);
		}
	}
	else if(strstr(reg, "Direction")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "HDTFThreshLSB") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::HDTFThreshLSB, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::HDTFThreshLSB, cmd->value);
			}
			else if(strstr(field, "SDTFThreshLSB") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::SDTFThreshLSB, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::SDTFThreshLSB, cmd->value);
			}
			else if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "DTFN") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::DTFN, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::DTFN, cmd->value);
			}
			else if(strstr(field, "DTFP") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::DTFP, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::DTFP, cmd->value);
			}
			else if(strstr(field, "DLOS") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Direction, DirectionRegister::DLOS, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Direction, DirectionRegister::DLOS, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Direction, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Direction, cmd->value);
		}
	}
	else if(strstr(reg, "OutputCTRL") !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "FLOSOF") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::FLOSOF, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::FLOSOF, cmd->value);
			}
			else if(strstr(field, "FLOSON") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::FLOSON, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::FLOSON, cmd->value);
			}
			else if(strstr(field, "LOSEN") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::LOSEN, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::LOSEN, cmd->value);
			}
			else if(strstr(field, "MUTE") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::MUTE, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::MUTE, cmd->value);
			}
			else if(strstr(field, "SDTFThesh") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::OutputCTRL, OutputCTRLRegister::SDTFThresh, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::OutputCTRL, OutputCTRLRegister::SDTFThresh, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::OutputCTRL, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::OutputCTRL, cmd->value);
		}
	}
	else if(strstr(reg, "Output") !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "HDTFThresh") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Output, OutputRegister::HDTFThresh, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Output, OutputRegister::HDTFThresh, cmd->value);
			}
			else if(strstr(field, "AMP") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Output, OutputRegister::AMP, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Output, OutputRegister::AMP, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Output, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Output, cmd->value);
		}
	}
	else if(strstr(reg, "RSVD06") !=NULL){
		if(read)
		error = TWI_manager->readRegister(Register::RSVD06, &cmd->value);
		else
		error = TWI_manager->writeRegister(Register::RSVD06, cmd->value);
	}
	else if(strstr(reg, "RSVD07") !=NULL){
		if(read)
		error = TWI_manager->readRegister(Register::RSVD07, &cmd->value);
		else
		error = TWI_manager->writeRegister(Register::RSVD07, cmd->value);
	}
	else if(strstr(reg, "Test")  !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "CMPCMD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Test, TestRegister::CMPCMD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Test, TestRegister::CMPCMD, cmd->value);
			}
			else if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Test, TestRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Test, TestRegister::RSVD, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Test, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Test, cmd->value);
		}
	}
	else if(strstr(reg, "Rev") !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Rev, RevRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Rev, RevRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "DIREV") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Rev, RevRegister::DIREV, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Rev, RevRegister::DIREV, cmd->value);
			}
			else if(strstr(field, "PARTID") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::Rev, RevRegister::PARTID, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::Rev, RevRegister::PARTID, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::Rev, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::Rev, cmd->value);
		}
	}
	else if(strstr(reg, "TFPCount")  !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "TFPCount.RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::TFPCount, TFPCountRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::TFPCount, TFPCountRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "TFPCount.TFPCount") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::TFPCount, TFPCountRegister::TFPCount, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::TFPCount, TFPCountRegister::TFPCount, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::TFPCount, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::TFPCount, cmd->value);
		}
	}
	else if(strstr(reg, "TFNCount") !=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "RSVD") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::TFNCount, TFNCountRegister::RSVD, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::TFNCount, TFNCountRegister::RSVD, cmd->value);
			}
			else if(strstr(field, "TFNCount") !=NULL){
				if(read)
				error = TWI_manager->getRegisterField(Register::TFNCount, TFNCountRegister::TFNCount, &cmd->value);
				else
				error = TWI_manager->setRegisterField(Register::TFNCount, TFNCountRegister::TFNCount, cmd->value);
			}
		}
		else {
			if(read)
			error = TWI_manager->readRegister(Register::TFNCount, &cmd->value);
			else
			error = TWI_manager->writeRegister(Register::TFNCount, cmd->value);
		}
	}
	delete empty;
	return error;
}


int SDI_Manager::parseCommandLMH0384(Command* cmd){
	int error = 0;
	char* reg = cmd->reg;
	char* field = cmd->reg_field;
	bool read = cmd->read;
	char* empty = (char*)malloc(REG_NAME_LENGTH);
	
	//Set SPI_manager chip and call read/write functions
	int chipID = cmd->chipID;
	if(chipID < 0 || chipID >= 5) return -51;
	lmh0384_SPI_manager->setChipID(cmd->chipID);
	
	if(strstr(reg, "General Control")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "Carrier Detect") !=NULL){
				
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->CARRIER_DETECT, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->CARRIER_DETECT, &cmd->value);
				}
			}
			else if(strstr(field, "Mute") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->MUTE, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->MUTE, &cmd->value);
				}
			}
			else if(strstr(field, "Bypass") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->BYPASS, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->BYPASS, &cmd->value);
				}
			}
			else if(strstr(field, "Sleep Mode") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->SLEEP_MODE, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->SLEEP_MODE, &cmd->value);
				}
			}
			else if(strstr(field, "Extended 3G Reach Mode") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->EXTENDED_3G_REACH_MODE, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->EXTENDED_3G_REACH_MODE, &cmd->value);
				}
			}
		}
		else {
			if(read)
			{
				error = lmh0384_SPI_manager->readRegister(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, &cmd->value);
			}
			else
			{
				error = lmh0384_SPI_manager->writeRegister(lmh0384_SPI_manager->lowApiMgr->regs->GENERAL_CONTROL_REGISTER_ADDRESS, &cmd->value);
			}
		}
	}
	else if(strstr(reg, "Output Driver")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "Output Swing") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_SWING, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_SWING, &cmd->value);
				}
			}
			else if(strstr(field, "Offset Voltage") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->OFFSET_VOLTAGE, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->OFFSET_VOLTAGE, &cmd->value);
				}
			}
		}
		else {
			if(read)
			{
				error = lmh0384_SPI_manager->readRegister(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, &cmd->value);
			}
			else
			{
				error = lmh0384_SPI_manager->writeRegister(lmh0384_SPI_manager->lowApiMgr->regs->OUTPUT_DRIVER_REGISTER_ADDRESS, &cmd->value);
			}
		}
	}
	else if(strstr(reg, "Launch Amplitude")!=NULL){
		if(strcmp(field, empty) != 0){
			if(strstr(field, "Coarse Control") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->COARSE_CONTROL, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->COARSE_CONTROL, &cmd->value);
				}
			}
			else if(strstr(field, "Fine Control") !=NULL){
				if(read)
				{
					error = lmh0384_SPI_manager->readRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->FINE_CONTROL, &cmd->value);
				}
				else
				{
					error = lmh0384_SPI_manager->writeRegisterField(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, lmh0384_SPI_manager->lowApiMgr->regs->FINE_CONTROL, &cmd->value);
				}
			}
		}
		else {
			if(read)
			{
				error = lmh0384_SPI_manager->readRegister(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS,&cmd->value);
			}
			else
			{
				error = lmh0384_SPI_manager->writeRegister(lmh0384_SPI_manager->lowApiMgr->regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, &cmd->value);
			}
		}
	}
	else if(strstr(reg, "CLI")!=NULL){
		if(read)
		{
			error = lmh0384_SPI_manager->readRegister(lmh0384_SPI_manager->lowApiMgr->regs->CLI_REGISTER_ADDRESS, &cmd->value);
		}
		else
		{
			error = lmh0384_SPI_manager->writeRegister(lmh0384_SPI_manager->lowApiMgr->regs->CLI_REGISTER_ADDRESS, &cmd->value);
		}
	}
	else if(strstr(reg, "Device ID") !=NULL){
		if(read)
		{
			error = lmh0384_SPI_manager->readRegister(lmh0384_SPI_manager->lowApiMgr->regs->DEVICE_ID_REGISTER_ADDRESS, &cmd->value);
		}
		else
		{
			error = lmh0384_SPI_manager->writeRegister(lmh0384_SPI_manager->lowApiMgr->regs->DEVICE_ID_REGISTER_ADDRESS, &cmd->value);
		}
	}
	
	delete empty;
	return error;
}


// default destructor
/*
SDI_Manager::~SDI_Manager()
{
	delete TWI_manager;
	delete lmh0384_SPI_manager;
}
*/
