/* 
* SDI_Manager.h
*
* Created: 10/27/2016 8:43:45 AM
* Author: darya
*/


#ifndef __SDI_MANAGER_H__
#define __SDI_MANAGER_H__

#include "../LMH0303/Manager/LMH0303_Manager.h"
#include "../LMH0384/Chips/LMH0384_Chip/LMH0384_Chip.h"
#include "../Command/Command.h"

#include <string.h>
#include <asf.h>
#define ANSWER_LENGTH 500

/*!
 * \brief Class of the manager for parsing and execute command.
 */
class SDI_Manager
 {
//variables
public:
protected:
private:
	static LMH0303_Manager* TWI_manager;			/**<Manager for reading from/writing to LMH0303. */
	static LMH0384_Chip* lmh0384_SPI_manager;		/**<Manager for reading from/writing to LMH0384. */
	
//functions
public:

	/**
	* Initialize SDI_Manager class. It must be called before creating an object of SDI_Manager class.
	*/	
	static void Initialize();
	/**
	* Set default values when the chips first come.
	* @return Number of an error or 0 if there is no error. -10 if no such register.
	*/
	static int setDefaults();
	/**
	* 
	* @param cmd Command that should be parsed.
	* @return Number an error: -49 no "read"/"write", -50 no valid chip, -51 no valid chip_id or no "all", -52 no valid register_name, -53 readonly register.
	*/
	static int parseCommand(Command* cmd);
protected:
private:
	/**
	* Set default values when the chips first come.
	* @param cmd Command that should be parsed.
	* @return Number an error: -49 no "read"/"write", -50 no valid chip, -51 no valid chip_id or no "all", -52 no valid register_name, -53 readonly register.
	*/
	static int parseCommandLMH0303(Command* cmd);
	static int parseCommandLMH0384(Command* cmd);
	static int setLMH0303Defaults();
	static int setLMH0384Defaults();
	//SDI_Manager( const SDI_Manager &c );
	//SDI_Manager& operator=( const SDI_Manager &c );

}; //SDI_Manager

#endif //__SDI_MANAGER_H__
