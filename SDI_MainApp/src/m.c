/*
 * m.c
 *
 * Created: 12/15/2016 3:41:55 PM
 *  Author: darya
 */ 


#include "m.h"

static volatile bool main_b_cdc_enable = false;


void read(char buf[], int size)
{
	if (udi_cdc_is_rx_ready()) {		
		// blocks until size bytes are received
		udi_cdc_read_buf(buf, size);
		
	}
}

void write(char buf[], int size)
{
	if (udi_cdc_is_rx_ready()) {
		
		udi_cdc_write_buf(buf, size);
		
	}
}


void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_cdc_enable)
		return;
	ui_process(udd_get_frame_number());
}

#ifdef USB_DEVICE_LPM_SUPPORT
void main_suspend_lpm_action(void)
{
	ui_powerdown();
}

void main_remotewakeup_lpm_disable(void)
{
	ui_wakeup_disable();
}

void main_remotewakeup_lpm_enable(void)
{
	ui_wakeup_enable();
}
#endif

bool main_cdc_enable(uint8_t port)
{
	main_b_cdc_enable = true;
	// Open communication
	uart_open(port);
	return true;
}

void main_cdc_disable(uint8_t port)
{
	main_b_cdc_enable = false;
	// Close communication
	uart_close(port);
}

void main_cdc_set_dtr(uint8_t port, bool b_enable)
{
	if (b_enable) {
		// Host terminal has open COM
		ui_com_open(port);
	}else{
		// Host terminal has close COM
		ui_com_close(port);
	}
}
