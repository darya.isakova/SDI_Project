/*
* new.cpp
*
* Created: 11/8/2016 9:10:43 AM
*  Author: darya
*/
#include "new.h"

void * operator new(size_t size)
{
	return malloc(size);
}

void operator delete(void * ptr)
{
	if(ptr)
	free(ptr);
}